.. |release| replace:: 3005.1
.. |supported-release-2| replace:: 3004.2
.. |release-badge| replace:: :bdg-link-success:`3005.1 <https://docs.saltproject.io/en/latest/topics/releases/index.html>`
.. |supported-release-2-badge| replace:: :bdg-link-primary:`3004.2 <https://docs.saltproject.io/en/3004/topics/releases/3004.2.html>`
.. |juniper-file-version| replace:: 20210827-213932
.. |native-minion-python-version| replace:: Python 3.7.10

.. |amazon-linux2-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/3005.repo
.. |amazon-linux2-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/minor/3005.1-1.repo

.. |amazon-linux2-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-classic-latest-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-classic-major-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-classic-major-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/3005.repo
.. |amazon-linux2-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/archive/3005.1/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-classic-minor-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/archive/3005.1.repo

.. |centos9-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest/SALTSTACK-GPG-KEY2.pub
.. |centos9-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |centos9-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3005/SALTSTACK-GPG-KEY2.pub
.. |centos9-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3005.repo
.. |centos9-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY2.pub
.. |centos9-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3005.1-1.repo

.. |centos8-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |centos8-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |centos8-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |centos8-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3005.repo
.. |centos8-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY.pub
.. |centos8-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3005.1-1.repo

.. |centos7-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |centos7-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |centos7-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |centos7-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3005.repo
.. |centos7-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY.pub
.. |centos7-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3005.1-1.repo

.. |debian11-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/latest/salt-archive-keyring.gpg
.. |debian11-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/latest bullseye main
.. |debian11-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/3005/salt-archive-keyring.gpg
.. |debian11-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/3005 bullseye main
.. |debian11-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/minor/3005.1-1/salt-archive-keyring.gpg
.. |debian11-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/minor/3005.1-1 bullseye main

.. |debian10-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/latest/salt-archive-keyring.gpg
.. |debian10-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/latest buster main
.. |debian10-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/3005/salt-archive-keyring.gpg
.. |debian10-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/3005 buster main
.. |debian10-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/minor/3005.1-1/salt-archive-keyring.gpg
.. |debian10-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/minor/3005.1-1 buster main

.. |debian11-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-major-download| replace:: LINK UNAVAILABLE AFTER UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE

.. |debian11-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/latest/salt-archive-keyring.gpg
.. |debian11-classic-latest-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/latest bullseye main
.. |debian11-classic-major-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/3005/salt-archive-keyring.gpg
.. |debian11-classic-major-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/3005 bullseye main
.. |debian11-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/archive/3005.1/salt-archive-keyring.gpg
.. |debian11-classic-minor-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/archive/3005.1 bullseye main

.. |debian11-arm64-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/latest/salt-archive-keyring.gpg
.. |debian11-arm64-classic-latest-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/latest bullseye main
.. |debian11-arm64-classic-major-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/3005/salt-archive-keyring.gpg
.. |debian11-arm64-classic-major-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/3005 bullseye main
.. |debian11-arm64-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/archive/3005.1/salt-archive-keyring.gpg
.. |debian11-arm64-classic-minor-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/archive/3005.1 bullseye main

.. |debian10-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/latest/salt-archive-keyring.gpg
.. |debian10-classic-latest-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/latest buster main
.. |debian10-classic-major-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/3005/salt-archive-keyring.gpg
.. |debian10-classic-major-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/3005 buster main
.. |debian10-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/archive/3005.1/salt-archive-keyring.gpg
.. |debian10-classic-minor-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/archive/3005.1 buster main

.. |macos-onedir-amd64-download| replace:: https://repo.saltproject.io/salt/py3/macos/3005.1-1/salt-3005.1-1-macos-x86_64.pkg
.. |macos-onedir-amd64-gpg| replace:: https://repo.saltproject.io/salt/py3/macos/3005.1-1/salt-archive-keyring.gpg

.. |macos-classic-amd64-download| replace:: https://repo.saltproject.io/osx/salt-3005.1-1-py3-x86_64.pkg
.. |macos-classic-amd64-md5| replace:: https://repo.saltproject.io/osx/salt-3005.1-1-py3-x86_64.pkg.md5
.. |macos-classic-amd64-sha256| replace:: https://repo.saltproject.io/osx/salt-3005.1-1-py3-x86_64.pkg.sha256

.. |photonos3-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE

.. |raspbian11-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/11/armhf/latest/salt-archive-keyring.gpg
.. |raspbian11-classic-latest-download| replace:: https://repo.saltproject.io/py3/debian/11/armhf/latest bullseye main
.. |raspbian11-classic-major-gpg| replace:: https://repo.saltproject.io/py3/debian/11/armhf/3005/salt-archive-keyring.gpg
.. |raspbian11-classic-major-download| replace:: https://repo.saltproject.io/py3/debian/11/armhf/3005 bullseye main
.. |raspbian11-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/11/armhf/archive/3005.1/salt-archive-keyring.gpg
.. |raspbian11-classic-minor-download| replace:: https://repo.saltproject.io/py3/debian/11/armhf/archive/3005.1 bullseye main

.. |raspbian10-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/10/armhf/latest/salt-archive-keyring.gpg
.. |raspbian10-classic-latest-download| replace:: https://repo.saltproject.io/py3/debian/10/armhf/latest buster main
.. |raspbian10-classic-major-gpg| replace:: https://repo.saltproject.io/py3/debian/10/armhf/3005/salt-archive-keyring.gpg
.. |raspbian10-classic-major-download| replace:: https://repo.saltproject.io/py3/debian/10/armhf/3005 buster main
.. |raspbian10-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/10/armhf/archive/3005.1/salt-archive-keyring.gpg
.. |raspbian10-classic-minor-download| replace:: https://repo.saltproject.io/py3/debian/10/armhf/archive/3005.1 buster main

.. |rhel9-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest/SALTSTACK-GPG-KEY2.pub
.. |rhel9-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel9-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3005/SALTSTACK-GPG-KEY2.pub
.. |rhel9-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3005.repo
.. |rhel9-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY2.pub
.. |rhel9-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3005.1-1.repo

.. |rhel8-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel8-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |rhel8-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |rhel8-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3005.repo
.. |rhel8-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY.pub
.. |rhel8-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3005.1-1.repo

.. |rhel8-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel8-classic-latest-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/latest.repo
.. |rhel8-classic-major-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |rhel8-classic-major-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/3005.repo
.. |rhel8-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/archive/3005.1/SALTSTACK-GPG-KEY.pub
.. |rhel8-classic-minor-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/archive/3005.1.repo

.. |rhel7-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel7-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |rhel7-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |rhel7-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3005.repo
.. |rhel7-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3005.1-1/SALTSTACK-GPG-KEY.pub
.. |rhel7-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3005.1-1.repo

.. |rhel7-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel7-classic-latest-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/latest.repo
.. |rhel7-classic-major-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/3005/SALTSTACK-GPG-KEY.pub
.. |rhel7-classic-major-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/3005.repo
.. |rhel7-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/archive/3005.1/SALTSTACK-GPG-KEY.pub
.. |rhel7-classic-minor-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/archive/3005.1.repo

.. |ubuntu22-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu22-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/latest jammy main
.. |ubuntu22-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/3005/salt-archive-keyring.gpg
.. |ubuntu22-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/3005 jammy main
.. |ubuntu22-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/minor/3005.1-1/salt-archive-keyring.gpg
.. |ubuntu22-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/minor/3005.1-1 jammy main

.. |ubuntu22-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE AFTER UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-major-download| replace:: LINK UNAVAILABLE AFTER UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu20-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu20-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/3005/salt-archive-keyring.gpg
.. |ubuntu20-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/3005 focal main
.. |ubuntu20-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/minor/3005.1-1/salt-archive-keyring.gpg
.. |ubuntu20-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/minor/3005.1-1 focal main

.. |ubuntu20-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu20-classic-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-classic-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3005/salt-archive-keyring.gpg
.. |ubuntu20-classic-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3005 focal main
.. |ubuntu20-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/archive/3005.1/salt-archive-keyring.gpg
.. |ubuntu20-classic-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/archive/3005.1 focal main

.. |ubuntu20-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT AFTER 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT AFTER 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL AFTER SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu20-arm64-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/latest/salt-archive-keyring.gpg
.. |ubuntu20-arm64-classic-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/latest focal main
.. |ubuntu20-arm64-classic-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/3005/salt-archive-keyring.gpg
.. |ubuntu20-arm64-classic-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/3005 focal main
.. |ubuntu20-arm64-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/archive/3005.1/salt-archive-keyring.gpg
.. |ubuntu20-arm64-classic-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/archive/3005.1 focal main

.. |ubuntu18-onedir-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu18-onedir-latest-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/latest bionic main
.. |ubuntu18-onedir-major-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/3005/salt-archive-keyring.gpg
.. |ubuntu18-onedir-major-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/3005 bionic main
.. |ubuntu18-onedir-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/minor/3005.1-1/salt-archive-keyring.gpg
.. |ubuntu18-onedir-minor-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/18.04/amd64/minor/3005.1-1 bionic main

.. |ubuntu18-classic-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu18-classic-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/latest bionic main
.. |ubuntu18-classic-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/3005/salt-archive-keyring.gpg
.. |ubuntu18-classic-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/3005 bionic main
.. |ubuntu18-classic-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/archive/3005.1/salt-archive-keyring.gpg
.. |ubuntu18-classic-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/archive/3005.1 bionic main

.. |windows-install-exe-example| replace:: salt-3005.1-1-windows-amd64.exe
.. |windows-install-msi-example| replace:: Salt-Minion-3005.1-1-Py3-AMD64.msi.md5

.. |windows-onedir-amd64-exe-download| replace:: https://repo.saltproject.io/salt/py3/windows/3005.1-1/salt-3005.1-1-windows-amd64.exe
.. |windows-onedir-amd64-exe-gpg| replace:: https://repo.saltproject.io/salt/py3/windows/3005.1-1/salt-archive-keyring.gpg

.. |windows-classic-amd64-msi-download| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64.msi
.. |windows-classic-amd64-msi-md5| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64.msi.md5
.. |windows-classic-amd64-msi-sha256| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64.msi.sha256

.. |windows-classic-amd64-exe-download| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64-Setup.exe
.. |windows-classic-amd64-exe-md5| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64-Setup.exe.md5
.. |windows-classic-amd64-exe-sha256| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-AMD64-Setup.exe.sha256

.. |windows-classic-x86-msi-download| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86.msi
.. |windows-classic-x86-msi-md5| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86.msi.md5
.. |windows-classic-x86-msi-sha256| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86.msi.sha256

.. |windows-classic-x86-exe-download| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86-Setup.exe
.. |windows-classic-x86-exe-md5| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86-Setup.exe.md5
.. |windows-classic-x86-exe-sha256| replace:: https://repo.saltproject.io/windows/Salt-Minion-3005.1-1-Py3-x86-Setup.exe.sha256
