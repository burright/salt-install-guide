.. _downloads:

=========
Downloads
=========

.. sidebar:: **Supported Salt releases**

    |release-badge| |supported-release-2-badge|

    **See also**

    * :ref:`salt-supported-operating-systems`
    * :ref:`salt-version-support-lifecycle`
    * :ref:`salt-python-version-support`


Welcome to the Salt download page!

* Salt packages are hosted on `repo.saltproject.io <https://repo.saltproject.io/>`_.
* Unsupported versions can be found in the `PY2 archive repository <https://archive.repo.saltproject.io/salt/py2/>`__
  and the `PY3 archive repository <https://archive.repo.saltproject.io/salt/py3/>`_.

The following sections provide links to the repository for each operating
system as well as links to the installation instructions for each operating
system.

.. Tip::
    As a security best practice, always verify that you have downloaded Salt
    from the authorized site by comparing the SHA/MD5 of the package or by
    verifying the package URL is an official Salt repository.


AIX
===
The AIX native minion package is a tarball containing an installation and
removal script and an AIX bff package.

.. grid:: 2

    .. grid-item-card:: Browse the repo for AIX packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/salt/py3/aix/

        :bdg-danger:`AIX`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: AIX install guide
        :class-card: sd-border-1
        :link: install-aix
        :link-type: ref

        :bdg-info:`Install Salt on AIX`


.. warning::

    There are separate builds for AIX v7.1 and AIX v7.2.


Amazon Linux 2
==============
Install the Amazon Linux 2 package using a Linux package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Amazon Linux 2 packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/py3/amazon/

        :bdg-danger:`Amazon Linux 2`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Amazon Linux 2 install guide
        :class-card: sd-border-1
        :link: install-amazon
        :link-type: ref

        :bdg-info:`Install Salt on Amazon Linux 2`


Arista
======
The Arista native minion package is a SWIX file.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Arista packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/salt/py3/arista/

        :bdg-danger:`Arista`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Arista install guide
        :class-card: sd-border-1
        :link: install-arista
        :link-type: ref

        :bdg-info:`Install Salt on Arista`


CentOS
======
Install the CentOS package using a yum package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for CentOS packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/py3/redhat/

        :bdg-danger:`CentOS`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: CentOS install guide
        :class-card: sd-border-1
        :link: install-centos
        :link-type: ref

        :bdg-info:`Install Salt on CentOS`


Debian
======
Install the Debian package using an apt package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Debian packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/py3/debian/

        :bdg-danger:`Debian`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Debian install guide
        :class-card: sd-border-1
        :link: install-debian
        :link-type: ref

        :bdg-info:`Install Salt on Debian`



Fedora
======
Salt packages for Fedora are hosted on the Fedora package repository. Install
the Fedora packages from the Fedora package manager.

.. card:: Fedora install guide
    :class-card: sd-border-1
    :link: install-fedora
    :link-type: ref
    :width: 50%

    :bdg-info:`Install Salt on Fedora`


Juniper
=======
The Juniper native minion| package is a tarball (.tgz) file.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Juniper packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/salt/py3/juniper/

        :bdg-danger:`Juniper`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Juniper install guide
        :class-card: sd-border-1
        :link: install-juniper
        :link-type: ref

        :bdg-info:`Install Salt on Juniper`


macOS
=====
Download the macOS package and run the file to install Salt. See the
:ref:`install-macos` install guide for more information.

.. include:: _includes/macos-downloads.rst

.. grid:: 2

    .. grid-item-card:: Browse the repo for macOS packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/osx/

        :bdg-danger:`macOS`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: macOS install guide
        :class-card: sd-border-1
        :link: install-macos
        :link-type: ref

        :bdg-info:`Install Salt on macOS`


Photon OS
=========
Salt packages for Photon OS are hosted on the Photon OS package repository.
Install the Photon OS packages from the Photon OS package manager.

.. card:: Photon OS install guide
    :class-card: sd-border-1
    :link: install-photonos
    :link-type: ref
    :width: 50%

    :bdg-info:`Install Salt on Photon OS`


Raspbian
========
Use the same download and installation instructions as `Debian`_.


RedHat
======
Install the RedHat package using a yum package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for RedHat (RHEL) packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/py3/redhat/

        :bdg-danger:`RedHat`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: RedHat install guide
        :class-card: sd-border-1
        :link: install-rhel
        :link-type: ref

        :bdg-info:`Install Salt on RedHat`


SUSE (SLES)
===========
Install the SUSE (SLES) package from the SUSE package repository. SUSE creates
its own Salt packages and the Salt Project does not publish separate Salt
packages for download.

.. grid:: 2

    .. grid-item-card:: SUSE (SLES) install guide
        :class-card: sd-border-1
        :link: install-sles
        :link-type: ref

        :bdg-info:`Install Salt on SUSE (SLES)`


Solaris
=======
The Solaris native minion package is available in the ``p5p`` format or as a
tarball (.tgz) file.


.. grid:: 2

    .. grid-item-card:: Browse the repo for Solaris packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/salt/py3/solaris/

        :bdg-danger:`Solaris`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Solaris install guide
        :class-card: sd-border-1
        :link: install-solaris
        :link-type: ref

        :bdg-info:`Install Salt on Solaris`


Ubuntu
======
Install the Ubuntu package using an apt package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Ubuntu packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/py3/ubuntu/

        :bdg-danger:`Ubuntu`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Ubuntu install guide
        :class-card: sd-border-1
        :link: install-ubuntu
        :link-type: ref

        :bdg-info:`Install Salt on Ubuntu`


Windows
=======
Download the Windows package and run the file to install Salt. See the
:ref:`install-windows` install guide for more information.

.. include:: _includes/windows-downloads.rst

.. grid:: 2

    .. grid-item-card:: Browse the repo for Windows packages
        :class-card: sd-border-1
        :link: https://repo.saltproject.io/windows/

        :bdg-danger:`Windows`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-2-badge|

    .. grid-item-card:: Windows install guide
        :class-card: sd-border-1
        :link: install-windows
        :link-type: ref

        :bdg-info:`Install Salt on Windows`
