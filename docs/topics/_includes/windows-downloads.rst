.. list-table::
  :widths: 10 10 10 70
  :header-rows: 1
  :class: windows-mac-download

  * - Package
    - Arch
    - File type
    - Download

  * - onedir
    - AMD64
    - exe
    -  **Download the install file**

       * |windows-onedir-amd64-exe-download|

       **GPG**

       * |windows-onedir-amd64-exe-gpg|

  * - classic
    - AMD64
    - msi
    -  **Download the install file**

       * |windows-classic-amd64-msi-download|

       **MD5**

       * |windows-classic-amd64-msi-md5|

       **SHA256**

       * |windows-classic-amd64-msi-sha256|

  * - classic
    - AMD64
    - exe
    -  **Download the install file**

       * |windows-classic-amd64-exe-download|

       **MD5**

       * |windows-classic-amd64-exe-md5|

       **SHA256**

       * |windows-classic-amd64-exe-sha256|

  * - classic
    - x86
    - msi
    -  **Download the install file**

       * |windows-classic-x86-msi-download|

       **MD5**

       * |windows-classic-x86-msi-md5|

       **SHA256**

       * |windows-classic-x86-msi-sha256|

  * - classic
    - x86
    - exe
    -  **Download the install file**

       * |windows-classic-x86-exe-download|

       **MD5**

       * |windows-classic-x86-exe-md5|

       **SHA256**

       * |windows-classic-x86-exe-sha256|
