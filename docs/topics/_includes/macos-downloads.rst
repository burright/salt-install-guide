.. list-table::
  :widths: 10 10 10 70
  :header-rows: 1
  :class: windows-mac-download

  * - Package
    - Arch
    - File type
    - Download

  * - onedir
    - x86_64
    - pkg
    -  **Download the install file**

       * |macos-onedir-amd64-download|

       **GPG**

       * |macos-onedir-amd64-gpg|

  * - classic
    - x86_64
    - pkg
    -  **Download the install file**

       * |macos-classic-amd64-download|

       **MD5**

       * |macos-classic-amd64-md5|

       **SHA256**

       * |macos-classic-amd64-sha256|
