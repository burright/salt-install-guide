.. admonition:: **What is onedir?**

   Onedir is the Salt Project’s new packaging system. Beginning with the release
   of Salt 3005 (Phosphorus), the Salt Project will begin replacing the old
   packaging system with the onedir packaging system.

   Onedir packages of Salt include the version of Python needed to run Salt and
   Salt's required dependencies.

   The Salt Project will support the old packaging system until the 3006
   release. See :ref:`upgrade-to-onedir` for more information.
